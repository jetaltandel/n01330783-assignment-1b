﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_1a_rewrite
{
    public class Client
    {
        private string clientFirstName;
        private string clientLastName;
        private string clientAddress;
        private string clientPostalCode;
        private string clientEmail;
        private int clientContactNumber;

        public Client()
        {
        }
        public string ClientFirstName
        {
            get { return clientFirstName; }
            set { clientFirstName = value; }
        }
        public string ClientLastName
        {
            get { return clientLastName; }
            set { clientLastName = value; }
        }
        public string ClientAddress
        {
            get { return clientAddress; }
            set { clientAddress = value; }
        }
        public string ClientPostalCode
        {
            get { return clientPostalCode; }
            set { clientPostalCode = value; }
        }
        public string ClientEmail
        {
            get { return clientEmail; }
            set { clientEmail = value; }
        }
        public int ClientContactNumber
        {
            get { return clientContactNumber; }
            set { clientContactNumber = value; }
        }
    }

}
