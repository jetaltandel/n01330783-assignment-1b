﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_1a_rewrite
{
    public class BookRoom
    {
        public RoomType roomtype;
        public Client client;
        public CheckInTime checkintime;

        public BookRoom(RoomType rt, Client c, CheckInTime ct)
        {
            roomtype = rt;
            client = c;
            checkintime = ct;
        }

        public string BookingSlip()
        {
            string slip = "Booking Slip:::    " + "<br/>";
            slip += "Here Your Payment Details:    " + CalculateBooking().ToString() + "<br/>";
            slip += "FirstName:     " + client.ClientFirstName + "<br/>";
            slip += "LastName:      " + client.ClientLastName + "<br/>";
            slip += "Address:      " + client.ClientAddress + "<br/>";
            slip += "PostalCode:    " + client.ClientPostalCode + "<br/>";
            slip += "Email:    " + client.ClientEmail + "<br/>";
            slip += "ContactNumber:    " +client.ClientContactNumber + "<br/>";
            slip += "Guest:    " + roomtype.guest + "<br/>";
            slip += "Booking Day:   " + roomtype.bookingday.ToString() + "<br/>";
            slip += "Facilities:   " + String.Join ("  ,  ", roomtype.facilities.ToArray())+ "<br/>";
            slip+= "CheckInTime:   " + checkintime.time + "<br/>";

            return slip;
        }
        public float CalculateBooking()
        {
            float amount = 0.00f;
            if (roomtype.bookingday == 1 && roomtype.guest == "1")
            {

                amount = 100.50f;
            }
            else if (roomtype.bookingday == 1 && roomtype.guest == "2")
            {
                amount = 110.50f;
            }
            else if (roomtype.bookingday == 1 && roomtype.guest == "3")
            {
                amount = 120.50f;
            }
            else if (roomtype.bookingday == 2 && roomtype.guest == "1")
            {
                amount = 200.50f;
            }
            else if (roomtype.bookingday == 2 && roomtype.guest == "2")
            {
                amount = 210.50f;
            }
            else if (roomtype.bookingday == 2 && roomtype.guest == "3")
            {
                amount = 220.50f;
            }
            else if (roomtype.bookingday == 3 && roomtype.guest == "1")
            {
                amount = 300.50f;
            }
            else if (roomtype.bookingday == 3 && roomtype.guest == "2")
            {
                amount = 310.50f;

            }
            else if (roomtype.bookingday == 3 && roomtype.guest == "3")
            {
                amount = 320.50f;
            }

           return amount;
        }
    }
}